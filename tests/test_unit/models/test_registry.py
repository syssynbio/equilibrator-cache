# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


"""Verify the Registry class behavior."""


import pytest

import equilibrator_cache.models.registry as registry


@pytest.mark.parametrize(
    "kwargs",
    [
        {"namespace": "metanetx.compound", "pattern": r"MNXM\d{3}"},
        {
            "name": "with name",
            "namespace": "metanetx.compound",
            "pattern": r"MNXM\d{3}",
        },
        {
            "namespace": "metanetx.compound",
            "pattern": r"MNXM\d{3}",
            "identifier": "MIR:00000003",
        },
        {
            "namespace": "metanetx.compound",
            "pattern": r"MNXM\d{3}",
            "url": "https://identifiers.org/metanetx.compound",
        },
        {"namespace": "chebi", "pattern": r"CHEBI:\d{3}", "is_prefixed": True},
        {
            "namespace": "metanetx.compound",
            "pattern": r"MNXM\d{3}",
            "access_url": "https://identifiers.org/metanetx.compound",
        },
    ],
)
def test___init__(kwargs):
    """Ensure initialization occurs as expected."""
    reg = registry.Registry(**kwargs)
    for key, value in kwargs.items():
        assert getattr(reg, key) == value


@pytest.mark.parametrize(
    "kwargs, expected",
    [
        (
            {
                "namespace": "metanetx.compound",
                "pattern": r"MNXM\d{3}",
                "identifier": "MIR:00000001",
            },
            "Registry(namespace=metanetx.compound)",
        )
    ],
)
def test___repr__(kwargs, expected):
    """Ensure a consistent string representation."""
    reg = registry.Registry(**kwargs)
    assert repr(reg) == expected


@pytest.mark.parametrize(
    "kwargs, expected_exception",
    [
        (
            {
                "namespace": "metanetx.compound",
                "pattern": r"MNXM\d{3}",
                "identifier": "MIR:00000001",
            },
            None,
        ),
        (
            {
                "namespace": "metanetx.compound",
                "pattern": r"MNXM\d{3}",
                "identifier": "MIR:000002",
            },
            ValueError,
        ),
        (
            {
                "namespace": "metanetx.compound",
                "pattern": r"MNXM\d{3}",
                "identifier": "MIR:0000000003",
            },
            ValueError,
        ),
        (
            {
                "namespace": "metanetx.compound",
                "pattern": r"MNXM\d{3}",
                "identifier": "MIR:0000wefweeSFWE04",
            },
            ValueError,
        ),
    ],
)
def test_validate_identifier(kwargs, expected_exception):
    """Verify the registry's identifier validation."""
    identifier = kwargs["identifier"]
    if expected_exception is not None:
        with pytest.raises(expected_exception):
            reg = registry.Registry(**kwargs)
            assert reg.validate_identifier(None, identifier) == identifier
    else:
        reg = registry.Registry(**kwargs)
        assert reg.validate_identifier(None, identifier) == identifier


@pytest.mark.parametrize(
    "kwargs, accession, is_valid",
    [
        (
            {
                "namespace": "metanetx.compound",
                "pattern": r"^MNXM\d{3}$",
                "identifier": "MIR:00000001",
            },
            "MNXM001",
            True,
        ),
        (
            {
                "namespace": "metanetx.compound",
                "pattern": r"^MNXM\d{3}$",
                "identifier": "MIR:00000001",
            },
            "MNXM0001",
            False,
        ),
    ],
)
def test_is_valid_accession(kwargs, accession, is_valid):
    """Verify the registry's identifier validation."""
    reg = registry.Registry(**kwargs)
    assert reg.is_valid_accession(accession) == is_valid
