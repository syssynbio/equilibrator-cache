# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest

from equilibrator_cache.compound_cache import CompoundCache
from equilibrator_cache.models import CompoundIdentifier


@pytest.fixture(scope="module")
def mini_kegg_cache(engine):
    cache = CompoundCache(engine)
    identifiers = [
        "C00008",
        "C00002",
        "C00031",
        "C00001",
        "C00469",
        "C00020",
        "C00009",
    ]
    cache.session.bulk_insert_mappings(
        CompoundIdentifier,
        [
            {"compound_id": i, "registry_id": 1, "accession": a}
            for i, a in enumerate(identifiers, start=1)
        ],
    )
    cache.session.commit()
    return cache


@pytest.mark.parametrize("kwargs", [{}])
def test___init__(engine, kwargs):
    CompoundCache(engine=engine, **kwargs)


def test_all_compound_accessions(mini_kegg_cache):
    expected = [
        "C00001",
        "C00002",
        "C00008",
        "C00009",
        "C00020",
        "C00031",
        "C00469",
    ]
    assert mini_kegg_cache.all_compound_accessions() == expected
    expected.reverse()
    assert mini_kegg_cache.all_compound_accessions(ascending=False) == expected


@pytest.mark.parametrize(
    "accession, expected",
    [
        ("C00001", True),
        ("C00008", True),
        ("C00020", True),
        ("C00301", False),
        ("C00666", False),
        (None, False),
    ],
)
def test_accession_exists(mini_kegg_cache, accession, expected):
    assert mini_kegg_cache.accession_exists(accession) == expected
