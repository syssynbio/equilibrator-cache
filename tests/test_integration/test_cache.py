# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest


@pytest.mark.parametrize(
    ("accession", "exists"),
    [
        ("KEGG:C00001", True),
        ("KEGG:C00002", True),
        ("KEGG:C00003", True),
        ("KEGG:C00004", True),
        ("KEGG:C99999", False),
        ("CHEBI:42758", True),
    ],
)
def test_get_compound(ccache, accession, exists):
    c = ccache.get_compound(accession)
    if exists:
        assert c is not None
    else:
        assert c is None


@pytest.mark.parametrize(
    ("inchi", "inchi_key"),
    [
        ("InChI=1S/p+1", "GPRLSGONYQIRFK-UHFFFAOYSA"),
        ("InChI=1S/H2O/h1H2", "XLYOFNOQVPJJNP-UHFFFAOYSA"),
        (
            "InChI=1S/CH2O3/c2-1(3)4/h(H2,2,3,4)/p-1",
            "BVKZGUZCCUSVTD-UHFFFAOYSA",
        ),
        ("InChI=1S/O2/c1-2", "MYMOFIZGZYHOMD-UHFFFAOYSA"),
    ],
)
def test_get_compound_by_inchi(ccache, inchi, inchi_key):
    c = ccache.get_compound_by_inchi(inchi)
    assert c is not None

    c_list = ccache.search_compound_by_inchi_key(inchi_key)
    assert c in c_list


@pytest.mark.parametrize(
    ("accession", "is_proton", "is_water"),
    [
        ("KEGG:C00080", True, False),
        ("KEGG:C00001", False, True),
        ("KEGG:C00060", False, False),
    ],
)
def test_water_and_protons(ccache, accession, is_proton, is_water):
    cpd = ccache.get_compound(accession)
    assert ccache.is_proton(cpd) == is_proton
    assert ccache.is_water(cpd) == is_water
