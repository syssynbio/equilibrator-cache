# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pytest

from equilibrator_cache import Compound
from equilibrator_cache.reaction import Reaction
from equilibrator_cache.thermodynamic_constants import Q_, R, debye_hueckel


@pytest.fixture(scope="module")
def atp_comp(ccache):
    return ccache.get_compound("KEGG:C00002")


@pytest.fixture(scope="module")
def atp_hydrolysis_reaction(ccache) -> Reaction:
    formula = "KEGG:C00002 + KEGG:C00001 <=> KEGG:C00008 + KEGG:C00009"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.mark.parametrize(
    ("ionic_strength", "temperature", "expected_in_rt"),
    [
        (Q_(0.10, "M"), Q_(298.15, "K"), Q_(0.247)),
        (Q_(0.25, "M"), Q_(298.15, "K"), Q_(0.327)),
        (Q_(0.50, "M"), Q_(298.15, "K"), Q_(0.390)),
    ],
)
def test_thermodynamic_constants(ionic_strength, temperature, expected_in_rt):
    dh = debye_hueckel(ionic_strength, temperature)
    assert dh.check(None)
    assert dh / expected_in_rt == pytest.approx(1.0, rel=1e-2)


def test_transform(atp_comp):
    p_h = Q_(5.0)
    ionic_strength = Q_(0.5, "M")
    temperature = Q_(300.0, "K")
    expected_ddg_over_rts = [0, 72.3, 114.9, 143.2, 162.1, 171.0, 176.2]
    expected_ddg_over_rts = [
        Q_(114.9 - x, "kJ/mol") / (R * temperature)
        for x in expected_ddg_over_rts
    ]
    assert len(atp_comp.microspecies) == len(expected_ddg_over_rts)

    for ms, ddg_over_rt in zip(atp_comp.microspecies, expected_ddg_over_rts):
        assert ddg_over_rt - 0.1 < ms.ddg_over_rt < ddg_over_rt + 0.1

    list(
        map(
            lambda ms: -1.0 * ms.transform(p_h, ionic_strength, temperature),
            atp_comp.microspecies,
        )
    )


@pytest.mark.parametrize(
    ("p_h", "ionic_strength", "temperature", "expected_in_rt"),
    [
        (5.0, Q_(0.0, "M"), Q_(300, "K"), Q_(149.06)),
        (7.0, Q_(0.0, "M"), Q_(300, "K"), Q_(209.21)),
        (9.0, Q_(0.0, "M"), Q_(300, "K"), Q_(265.74)),
        (7.0, Q_(0.5, "M"), Q_(300, "K"), Q_(208.83)),
    ],
)
def test_atp_formation(
    p_h, ionic_strength, temperature, expected_in_rt, atp_comp
):
    assert type(atp_comp) == Compound

    ddg_over_rt = atp_comp.transform(p_h, ionic_strength, temperature)
    assert ddg_over_rt / expected_in_rt == pytest.approx(1.0, abs=1e-3)


@pytest.mark.parametrize(
    ("p_h", "ionic_strength", "temperature", "expected_in_rt"),
    [
        (5.0, Q_(0.0, "M"), Q_(300, "K"), Q_(-16.01)),
        (7.0, Q_(0.0, "M"), Q_(300, "K"), Q_(-16.76)),
        (9.0, Q_(0.0, "M"), Q_(300, "K"), Q_(-20.73)),
        (7.0, Q_(0.5, "M"), Q_(300, "K"), Q_(-15.62)),
    ],
)
def test_atp_hydrolisys(
    p_h, ionic_strength, temperature, expected_in_rt, atp_hydrolysis_reaction
):
    assert type(atp_hydrolysis_reaction) == Reaction

    ddg_over_rt = atp_hydrolysis_reaction.transform(
        p_h, ionic_strength, temperature
    )
    assert ddg_over_rt / expected_in_rt == pytest.approx(1.0, abs=1e-3)
